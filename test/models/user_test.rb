require 'test_helper'

class UserTest < ActiveSupport::TestCase
 def setup
 	@user = User.new(name: "Some User", email: "someuser@gmail.com", password: "foobar", password_confirmation: "foobar")
 end

 test "should be valid" do
 	assert @user.valid?
 end

 test "name should be present" do 
 	@user.name = "    "
 	assert_not @user.valid?
 end

 test "email should be present" do 
 	@user.email = "    "
 	assert_not @user.valid?
 end

 test "name should not be too long" do 
 	@user.name = "a" * 51
 	assert_not @user.valid?
 end

 test "email should not be too long" do 
 	@user.email = "a" * 246 + "@gmail.com"
 	assert_not @user.valid?
 end

 test "password should have a minimum length" do 
 	@user.password = @user.password_confirmation = 'a' * 5
 	assert_not @user.valid?
 end
 
 test "authenticated? should return false for a user with nil digest" do
  assert_not @user.authenticated?(:remember, '')
 end
 
 test "associated microposts should be destroyed" do
  @user.save
  @user.microposts.create!(content: "Lorem Ipsum")
  assert_difference 'Micropost.count', -1 do
   @user.destroy
  end
 end
 
 test "should follow and unfollow a user" do
  ravi = users(:ravi)
  champa = users(:champa)
  assert_not ravi.following?(champa)
  ravi.follow(champa)
  assert ravi.following?(champa)
  ravi.unfollow(champa)
  assert_not ravi.following?(champa)
 end
 
end