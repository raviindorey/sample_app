module ApplicationHelper

	def title_helper(title = '')
		if title.empty? then
			full_title = "Ruby on Rails Tutorial Sample App"
		else
			full_title = title.to_s + " | Ruby on Rails Tutorial Sample App"
		end
	end
end
