class ApplicationMailer < ActionMailer::Base
  default from: "ravi.indorey90@gmail.com"
  layout 'mailer'
end
